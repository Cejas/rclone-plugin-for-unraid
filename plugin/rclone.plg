<?xml version='1.0' standalone='yes'?>

<!DOCTYPE PLUGIN [
<!ENTITY name      		"rclone">
<!ENTITY author    		"Waseh/Cejas">
<!ENTITY version   		"2020.08.16">
<!ENTITY bundleversion   	"2019.10.13">
<!ENTITY launch    		"Settings/rclone">
<!ENTITY pluginURL 		"https://gitlab.com/Cejas/rclone-plugin-for-unraid/-/raw/master/plugin/rclone.plg">
<!ENTITY bundleURL  		"https://gitlab.com/Cejas/rclone-plugin-for-unraid/-/raw/master/archive/rclone-&bundleversion;-x86_64-1.txz">
<!ENTITY rcloneversion		"current">
<!ENTITY rclonefile  		"rclone-&rcloneversion;-linux-amd64">
<!ENTITY rcloneurl		"https://downloads.rclone.org/&rclonefile;.zip">
<!ENTITY md5bundle     		"f240277fc3ecbbff7774bb3ee80d695a">
]>

<PLUGIN name="&name;" author="&author;" version="&version;" launch="&launch;" pluginURL="&pluginURL;">

<CHANGES>
##&name;

###2020.08.16
- Reverting last change, but extracting rclone direct to sbin folder

###2020.08.15
- Saving rclone binary instead of the zip file

###2020.08.02
- Don't download rclone again if it's already downloaded
- Checks if user wants to use different rclone config before applying default

###2019.11.01
- Ensure wrapper updating
	
###2019.10.23
- Make rclone wrapper branch agnostic	
	
###2019.10.21
- Remove poorly documented logging option from rclone wrapper	
	
###2019.10.14
- Fusermount compatibility fix take two	
	
###2019.10.13b
- Fusermount compatibility fix for future unRaid versions	

###2019.10.13a
- Icon change and small corrections	
	
###2019.06.24
- Bump timeout again to alleviate problem with slow resolvers	
	
###2019.02.07
- Change timeout to alleviate problem with slow resolvers	
	
###2018.09.10c
- Extra connectivity test

###2018.09.10a
- Check if unpack was successful

###2018.09.10
- Test for internet connectivity
- Retry mechanism for curl

###2018.08.25
- Let install survive when no internet connection on boot

###2017.09.23
- Always pull newest version on install

###2017.08.16
- New version of rclone (v1.37)

###2017.03.19
- New version of rclone (v1.36)

###2017.01.17a
- Reverting previous patch

###2017.01.17
- Removed Wrapper

###2017.01.05
- New version of rclone (v1.35)

###2016.11.14
- Beta version of webgui
- Beta version of included template scripts

###2016.11.08
- Fixed update routine

###2016.11.06
- First release of plugin for rclone stable branch
- New version of rclone (v1.34)
- Check to see if Beta branch installed 

###2016.11.02
- More intuitive calling of rclone - Use rclone instead of myrclone
- More minor changes in preperation of official release

###2016.10.31
- Minor improvements

###2016.10.28
- Removed cronjob

###2016.10.27
- Small modifications to make the plugin work again, and updateable from unraid interface

</CHANGES>

<!--

This plugin installs Rclone on unRAID systems.
This work is entirely based upon the plugin created by aschamberger: https://lime-technology.com/forum/index.php?topic=46663.msg501372
Thanks to stignz for his great guide: https://lime-technology.com/forum/index.php?topic=46663.0

-->

<!--
Check if beta is installed.
-->
<FILE Run="/bin/bash">
<INLINE>
if [ -d /usr/local/emhttp/plugins/rclone-beta ]; then
echo ""
echo ""
echo "----------Beta Branch installed----------"
echo "Uninstall Beta branch to install Stable!"
echo ""
echo ""
exit 1
fi
</INLINE>
</FILE>


<!--
Check if fusermount is present
-->
<FILE Run="/bin/bash">
<INLINE>
if ! [ -f /bin/fusermount ]; then
  if [ -f /usr/bin/fusermount3 ]; then
    ln -s /usr/bin/fusermount3 /bin/fusermount
  fi;
fi;
</INLINE>
</FILE>


<!--
Get bundle.
-->
<FILE Name="/boot/config/plugins/&name;/install/rclone-&bundleversion;-bundle.txz" Run="upgradepkg --install-new">
<URL>&bundleURL;</URL>
<MD5>&md5bundle;</MD5>
</FILE>

<!--
Install script.
-->
<FILE Run="/bin/bash" Method="install">
<INLINE>

if [ ! -f /boot/config/plugins/&name;/install/rclone-&rcloneversion;.zip ]; then
  ping -q -c2 8.8.8.8 >/dev/null
  if [ $? -eq 0 ]
  then
    echo "Downloading rclone"
    curl --connect-timeout 8 --retry 3 --retry-delay 2 --retry-max-time 30 -o /boot/config/plugins/&name;/install/rclone-&rcloneversion;.zip &rcloneurl;

    echo "Downloading certs"
    curl --connect-timeout 8 --retry 3 --retry-delay 2 --retry-max-time 30 -o /boot/config/plugins/&name;/install/ca-certificates.new.crt https://curl.haxx.se/ca/cacert.pem
  else
    ping -q -c2 1.1.1.1 >/dev/null
    if [ $? -eq 0 ]
    then
      echo "Downloading rclone"
      curl --connect-timeout 15 --retry 3 --retry-delay 2 --retry-max-time 30 -o /boot/config/plugins/&name;/install/rclone-&rcloneversion;.zip &rcloneurl;

      echo "Downloading certs"
      curl --connect-timeout 15 --retry 3 --retry-delay 2 --retry-max-time 30 -o /boot/config/plugins/&name;/install/ca-certificates.new.crt https://curl.haxx.se/ca/cacert.pem
    else
      echo "No internet - Skipping download and using existing archives"
    fi
  fi
fi;

if [ -f /boot/config/plugins/&name;/install/ca-certificates.new.crt ]; then
  rm -f $(ls /boot/config/plugins/&name;/install/ca-certificates.crt 2>/dev/null)
  mv /boot/config/plugins/&name;/install/ca-certificates.new.crt /boot/config/plugins/&name;/install/ca-certificates.crt
fi;

rm -f $(ls /boot/config/plugins/&name;/install/rclone*.txz 2>/dev/null | grep -v '&bundleversion;')

if [ -f /boot/config/plugins/&name;/install/rclone-&rcloneversion;.zip ]; then
  unzip /boot/config/plugins/&name;/install/rclone-&rcloneversion;.zip rclone*/rclone -d /usr/sbin/rclonetemp
  if [ "$?" -ne "0" ]; then
    echo "Unpack failed - Please try installing/updating the plugin again. Be sure rclone is not running during installation."
    exit 1
  fi;
  rm -f /usr/sbin/rclone
  mv -f /usr/sbin/rclonetemp/rclone-v*/rclone /usr/sbin/rcloneorig
  rm -rf /usr/sbin/rclonetemp
else
  echo "Download failed - No existing archive found - Try again later"
  exit 1
fi;

chown root:root /usr/sbin/rcloneorig
chmod 755 /usr/sbin/rcloneorig

mkdir -p /etc/ssl/certs/
cp /boot/config/plugins/&name;/install/ca-certificates.crt /etc/ssl/certs/

if [ ! -f /boot/config/plugins/&name;/.rclone.conf ]; then
  touch /boot/config/plugins/&name;/.rclone.conf;
fi;

mkdir -p /boot/config/plugins/&name;/logs;
mkdir -p /boot/config/plugins/&name;/scripts;
cp /boot/config/plugins/&name;/install/scripts/* /boot/config/plugins/&name;/scripts/ -R -n;

mkdir -p /mnt/disks/;

echo ""
echo "-----------------------------------------------------------"
echo " &name; has been installed."
echo "-----------------------------------------------------------"
echo ""

</INLINE>
</FILE>

<FILE Run="/bin/bash" Method="remove">
<INLINE>
rm -rf /boot/config/plugins/&name;/install
rm -f /usr/sbin/rcloneorig;
rm -f /usr/sbin/rclone;
rm -f /etc/ssl/certs/ca-certificates.crt

removepkg rclone-&bundleversion;-bundle >/dev/null

# we keep config and logs
#rm -f /boot/config/plugins/&name;/.rclone.conf;
#rm -f /boot/config/plugins/&name;/logs;

echo ""
echo "-----------------------------------------------------------"
echo " &name; has been uninstalled."
echo "-----------------------------------------------------------"
echo ""

</INLINE>
</FILE>

<FILE Name="/usr/sbin/rclone" Mode="0755">
<INLINE>
#!/bin/bash
arguments="$@"
config=/boot/config/plugins/&name;/.rclone.conf
if [[ $arguments == *"--config"* ]]; then
  rcloneorig $arguments;
else
  rcloneorig --config $config $arguments;
fi	
</INLINE>
</FILE>

</PLUGIN>
